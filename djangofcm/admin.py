from django.contrib import admin
from .models import Device
# Register your models here.


class DeviceAdmin(admin.ModelAdmin):
    list_display = ['user', 'dev_id', 'reg_id']


admin.site.register(Device, DeviceAdmin)