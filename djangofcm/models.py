from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from django.db.models import Q

class DeviceManager(models.Manager):
    def get_for_user(self, user):
        qs = self.get_queryset()
        qs = qs.filter(user=user, is_active=True)
        return qs

    def get_for_users(self, users):
        qs = self.get_queryset()
        qs = qs.filter(user__in=users, is_active=True)
        return qs

    def get_for_existing(self, dev_id, reg_id):
        qs = self.get_queryset()
        qs = qs.filter(Q(dev_id=dev_id)|Q(reg_id=reg_id))
        device = qs.first()
        if not device:
            raise self.model.DoesNotExist
        return device

class Device(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False)
    dev_id = models.CharField(
        verbose_name="Device ID", max_length=50, unique=True,)
    reg_id = models.CharField(
        verbose_name="Registration ID", max_length=255, unique=True)
    name = models.CharField(
        verbose_name="Name", max_length=255, blank=True, null=True)
    creation_date = models.DateTimeField(
        verbose_name="Creation date", auto_now_add=True)
    modified_date = models.DateTimeField(
        verbose_name="Modified date", auto_now=True)
    is_active = models.BooleanField(
        verbose_name="Is active?", default=True)

    objects = DeviceManager()

    def __str__(self):
        return self.dev_id

    class Meta:
        verbose_name = "Device"
        verbose_name_plural = "Devices"
        ordering = ['-modified_date']

    def mark_inactive(self):
        self.is_active = False
