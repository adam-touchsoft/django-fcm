from rest_framework import serializers
from .models import Device
from django.db import IntegrityError


class DeviceSeri(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    dev_id = serializers.CharField(max_length=50)
    reg_id = serializers.CharField(max_length=255)
    name = serializers.CharField(max_length=255, required=False, allow_null=True)

    def update(self, instance, validated_data=None):
        if not validated_data:
            validated_data = self.validated_data
        instance.dev_id = validated_data['dev_id']
        instance.reg_id = validated_data['reg_id']
        instance.name = self.validated_data.get('name', None)
        instance.save()
        return instance

    def save(self, user, **kwargs):
        d = Device()
        d.user = user
        d.reg_id = self.validated_data['reg_id']
        d.dev_id = self.validated_data['dev_id']
        d.name = self.validated_data.get('name', None)

        try:
            d.save()
            return d
        except IntegrityError:
            d = Device.objects.get_for_existing(self.validated_data['dev_id'], self.validated_data['reg_id'])
            return self.update(d)



