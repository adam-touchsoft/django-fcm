from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^device/$', views.RegisterDeviceView.as_view(), name='register_device'),
    url(r'^device/(?P<id>[0-9])/$', views.UnregisterDeviceView.as_view(), name='unregister_device'),
]
