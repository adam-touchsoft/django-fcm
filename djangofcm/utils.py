from pyfcm import FCMNotification
from pyfcm.errors import FCMError
from django.conf import settings
from .models import Device


class NoDevicesFound(FCMError):
    message = 'no devices could be found'

push_service = FCMNotification(api_key=settings.FCM_API_KEY)


def send_message(users, **kwargs):

    devices = Device.objects.get_for_users(users=users)
    registration_ids = [d.reg_id for d in devices]

    if len(registration_ids) < 1:
        raise NoDevicesFound

    if len(registration_ids) < 2:
        result = push_service.notify_single_device(registration_id=registration_ids[0], **kwargs)
        results = [result]

    else:
        results = push_service.notify_multiple_devices(registration_ids=registration_ids, **kwargs)

    reg_ids_to_delete = []
    reg_ids_to_replace = []

    try:
        for result in results:
            if result['failure'] < 1:
                continue
            for reg_id, response in zip(registration_ids, result['results']):
                message_id = response.get('message_id', None)
                error = response.get('error', None)
                if message_id:
                    new_reg_id = response.get('registration_id', None)
                    if new_reg_id:
                        reg_ids_to_replace.append((reg_id, new_reg_id))
                elif error:
                    if error == 'NotRegistered':
                        reg_ids_to_delete.append(reg_id)

        Device.objects.filter(reg_id__in=reg_ids_to_delete).delete()

        for reg_id_to_replace, replacement_reg_id in reg_ids_to_replace:
            Device.objects.filter(reg_id=reg_id_to_replace).update(reg_id=replacement_reg_id)
    except Exception as e:
        pass

