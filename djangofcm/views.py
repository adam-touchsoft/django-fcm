"""
Provides a mixin class for permission handling.
Must be used together with View or a child of View
"""

from django.core.exceptions import PermissionDenied
from django.views.generic import View
from rest_framework.views import APIView
from .serializers import DeviceSeri
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .models import Device
from django.core.exceptions import PermissionDenied
from django.http import Http404


class RegisterDeviceView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = DeviceSeri

    def post(self, request, *args, **kwargs):
        """
        Register a Device
        ---
        serializer: DeviceSeri
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        seri = self.serializer_class(data=request.data)
        if seri.is_valid():
            device = seri.save(request.user)
            rseri = self.serializer_class(device)
            return Response(rseri.data, status=201)

        return Response(seri.errors, status=400)


class UnregisterDeviceView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = DeviceSeri

    def delete(self, request, *args, **kwargs):
        """
        Remove a device
        ---
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            device = Device.objects.get(id=kwargs['id'])
        except Device.DoesNotExist:
            raise Http404

        if device.user.id is not request.user.id:
            raise PermissionDenied

        device.mark_inactive()
        device.save()
        return Response(status=204)

    def put(self, request, *args, **kwargs):
        try:
            device = Device.objects.get(id=kwargs['id'])
        except Device.DoesNotExist:
            raise Http404

        if device.user.id is not request.user.id:
            raise PermissionDenied

        seri = self.serializer_class(data=request.data)
        if seri.is_valid():
            device = seri.update(device)
            rseri = self.serializer_class(device)
            return Response(rseri.data, status=200)

        return Response(seri.errors, status=400)