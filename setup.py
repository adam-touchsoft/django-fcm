import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.MD')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='djangofcm',
    version='0.1.12',
    packages=['djangofcm', 'djangofcm.migrations'],
    include_package_data=True,
    description='Google FCM Module',
    long_description=README,
    url='https://www.touch-soft.com/',
    author='Adam Godfrey',
    author_email='adam.godfrey@touchsoft.co.uk',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 1.9',  # replace "X.Y" as appropriate
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)